using System;
using System.Collections.Generic;

namespace netoExamen
{
    class Program
    {
        static void Main(string[] args)
        {
            // string result = AtmMachine(1200);
            // int[] result = fibonacci(11);
            List<string> result = sonPalindromos();
            Console.WriteLine(result);
        }

        public static int[] fibonacci(int n)
        {
            int prevNum = Convert.ToInt32(Math.Round(n / ((1 + Math.Sqrt(5)) / 2)));
            int nextNum = n + prevNum;

            int[] resp = { prevNum, n, nextNum };


            return resp;
        }

        public static List<string> sonPalindromos()
        {
            List<string> primos = new List<string>();
            for (int i = 1; i <= 1000; i++)
            {
                if (isPrimo(i))
                {
                    primos.Add(i.ToString());
                }
            }

            List<string> palindromos = new List<string>();

            foreach(string primo in primos)
            {
                char[] reverse = primo.ToCharArray();
                Array.Reverse(reverse);

                if (primo == new string(reverse))
                {
                    palindromos.Add(primo);
                }
            }

            return palindromos;
        }

        public static bool isPrimo(int num)
        {
            for (int i = 2; i < num; i++)
            {
                if ((num % i) == 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static string AtmMachine(int cash)
        {

            int[] billetes = { 500, 200, 100, 50 };
            string respuesta = "";
            int res = cash;

            if (cash < 50)
            {
                return "no se acepta esta denominacion";
            }
            else if ((cash % 50) != 0)
            {
                return "no se puede esa cantidad";
            }

            foreach(int n in billetes)
            {
                int cant = res / n;
                res = res % n;
                respuesta += cant + " billetes de " + n + " ";
            }

            return respuesta;
            
        }

        public void CodeRefactor()
        {
             var watch = System.Diagnostics.Stopwatch.StartNew();

                int y = 0, asistentes =  1000000000;

                y = asistentes - 2;

                float result = Factorial(asistentes) / Factorial(2) * Factorial(y);

                watch.Stop();

                Console.WriteLine($"El numero de saludos fúe {result}");

                Console.WriteLine($"Tardo  {watch.ElapsedMilliseconds}");
        }

        
    }
}
